/* get all paragraphs in the wiki article */
var p = document.querySelectorAll("#bodyContent .mw-parser-output > p");

/* this will be the text to check plagiarism */
var text = "";

/* if the paragraph has more than 30 characters, then
 * we'll pick random sentence from it with length between
 * 30-100 characters and add a splitting sequence
 * for further usage (---).
 */
for (i = 0; i < p.length; i++) {
  var t = p[i].innerText;
  if (t.length < 30) continue;
  t_arr = t.split(/\.[\s\n]{1}/);
  for (j = 0; j < t_arr.length; j++) {
    var t_snt = t_arr[Math.floor(Math.random() * t_arr.length)];
    if (t_snt.length > 30 && t_snt.length < 100) {
      text += t_snt + "---";
      continue;
    }
  }
}

/* remove references from the text (like this [1]) */
text = text.replace(/\[.*\]/g, "");