/* start this extension only at WikiSkripta page */
chrome.runtime.onInstalled.addListener(function() {
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    chrome.declarativeContent.onPageChanged.addRules([
      {
        conditions: [
          new chrome.declarativeContent.PageStateMatcher({
            pageUrl: { hostEquals: 'www.wikiskripta.eu', schemes: ['https'] },
          })
        ],
        actions: [ new chrome.declarativeContent.ShowPageAction() ]
      }
    ]);
  });
});

/* add onclick event for the extension page action */
chrome.pageAction.onClicked.addListener(function() {
  /* inject javascript to WikiSkripta
   * to get the source code and extract plain text
   * from the paragraphs
   */
  chrome.tabs.executeScript(
    {
      file: 'wikicontent.js'
    },
    function(text) {
      
      /* check if any content is returned */
      if(!text[0]) {
        window.alert(chrome.i18n.getMessage("errNoSentence"));
        return false;
      }
      
      /* we have to remove splitting sequence
       * (text[0] is because text is an object)
       */
      var urls = text[0].split("---");
      
      /* remove the last item (which is empty) */
      urls.splice(urls.length-1, 1);
      
      /* every sentence is formed to search URL */
      for (i = 0; i < urls.length; i++) {
        urls[i] = 'https://www.google.com/search?q="' + encodeURIComponent(urls[i]) + '"+-wikiskripta+-site%3Awikiskripta.eu';
      }
      
      /* create a new window with search tabs */
      chrome.windows.create({url: urls});
    }
  );
});

